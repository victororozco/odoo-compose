# Generador de Contenedores de Odoo10 con Balanceador de Carga:

## Requisitos:
- Python 2.7
- Docker v17
- Docker compose

## Uso
```bash
# python docker-odoo.py
```
Escoja la opción que mejor convenga para su caso:
```bash
a - Generar docker-compose.
b - Iniciar el contenedor de Odoo.
c - Detener los contenedores.
d - Eliminar los contenedores.
e - Usar balanceadores de carga
f - Listar contenedores
0 - Salir

Generador de contenedores de Odoo, que desea hacer?: 

```

## Tecnología usada:
- Docker
- Docker compose
- Python
- haproxy

## Características:

- Generar archivo docker-compose.
- Iniciar los contenedores.
- Detener los contenedores.
- Eliminar los contenedores.
- Balanceadores de carga (genera 'n' cantidad de contenedores odoo10 y envía las peticiones a cada contenedor de manera aleatoria).
- Listar contenedores.
- Instala docker-compose automaticamente.
- Copia la carpeta odoo10 al directorio opt de forma automatica y da los permisos adecuados.
- Al generar mas balaceadores no afecta a los contenedores en ejecucion.

**Nota:** Los contenedores que se crean son los administrados por este programa, los contenedores generados por docker y no por un docker-compose no serán tocados.

