# -*- coding: utf-8 -*-

###########################################
# Codigo escrito por Victor Orozco 		  #
# Twitter: @vorozco07					  #
# GitLab: https://gitlab.com/victororozco #
# GitHub: https://github.com/victororozco #
###########################################

import os
import subprocess

def verifyDockerCompose():

	for root, dirs, files in os.walk('.'):

		if 'docker-compose.yml' in files:

			try:
				subprocess.check_output('sudo docker-compose ps > /dev/null 2>&1', shell=True)
				configOdoo()
				return True

			except Exception as e:

				print('-------------------------------------')
				x = str(raw_input('\033[93mNo tienes instalado docker-compose, deseas instalarlo? [y/n]: \033[0m'))
				if x is 'y':
					try:
						subprocess.check_output('sudo curl -L https://github.com/docker/compose/releases/download/1.16.1/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose', shell=True)
						
						try:
							subprocess.check_output('sudo chmod +x /usr/local/bin/docker-compose', shell=True)
							return True
							
						except Exception as e:
							print('-------------------------------------')
							print('\033[91mERROR al asignar permisos: sudo chmod +x /usr/local/bin/docker-compose \033[0m')

					except Exception as e:
						print('-------------------------------------')
						print('\033[91mERROR al asignar permisos en: sudo chmod +x /usr/local/bin/docker-compose \033[0m')

		else:
			print('-------------------------------------')
			print('\033[93mNO se encontro el archivo docker-compose.yml, si ya tiene uno debe colocarlo en este directorio o Generar un nuevo docker-compose.\033[0m')
			p = str(raw_input('\033[92mDesea generar un nuevo docker-compose? [y/n]: \033[0m'))
			if p is 'y' or p is 'Y':
				if writeCompose() is True:
					verifyDockerCompose()
	
	return True



def writeCompose():
	structure = '''odoo10:
    image: odoo:10
    links: 
        - db10:db
    expose:
        - 8069
    volumes:
        - /opt/odoo10/addons:/mnt/extra-addons
        - /opt/odoo10/log:/var/log/odoo
        - /opt/odoo10/config:/etc/odoo

db10:
    image: postgres:9.4
    environment:
        - POSTGRES_USER=odoo
        - POSTGRES_PASSWORD=odoo

lb:
    image: tutum/haproxy
    links:
        - odoo10
    ports:
        - "8069:80"
    environment:
        - BACKEND_PORT=8069
        - BALACE = roundrobin
'''

	try:
		f = open('docker-compose.yml', 'w')
		f.write(structure)
		f.close()
		print('-------------------------------------')
		print('\033[92mdocker-compose.yml generado exitosamente.\033[0m')
		return True

	except:
		print('\033[91mERROR al generar docker-compose.yml, verifique que tenga accesos de escritura en el directorio. \033[0m')
		return False


def configOdoo():
	directory = False
	file = False

	for root, dirs, files in os.walk('/opt/'):

		if 'odoo10' in dirs:
			directory = True

			for root, dirs, files in os.walk('/opt/odoo10/config/'):
				if 'odoo.conf' in files:
					file = True

	if directory is False or file is False:
		try:
			subprocess.call('sudo cp -r odoo10 /opt/ && sudo chmod -R 755 /opt/odoo10', shell=True)
			print('\033[92mDirectorio copiado a "opt" exitosamente.\033[0m')
		except Exception as e:
			print('-------------------------------------')
			print('\033[91mERROR al copiar la carpeta odoo10 al directorio "opt", verifique que la carpeta odoo10 se encuentre en este directorio. \033[0m')
			exit()


def generateDockerCompose():
	writeDockerCompose = writeCompose()

	if writeDockerCompose is True:

		print('-------------------------------------')
		build = str(raw_input('\033[92mDesea lanzar los contenedores? [y/n]: \033[0m'))
		if build == 'y' or build == 'yes' or build == 'Y' or build == 'YES':

			try:
				subprocess.check_output('sudo docker-compose up -d', shell=True)
				print('-------------------------------------')
				result = str(raw_input('\033[92mDesea utilizar balancedores de carga? [y/n]: \033[0m'))

				if result is 'y' or result is 'Y':

					while True:
						try:

							print('-------------------------------------')
							num_lb = int(raw_input('\033[92mIndique el numero de balancedores (0 - salir): \033[0m'))
							if num_lb != 0:
								dockerBalance(num_lb)
							return False

						except Exception as e:
							print('\033[91mDebe ingresar un numero.\033[0m')

			except Exception as e:
				print('-------------------------------------')
				print('\033[91mNo se pudieron iniciar los contenedores.\033[0m')


def startOdoo():
	ready = False
	num_lb = 1

	for root, dirs, files in os.walk('.'):

		if 'docker-compose.yml' in files:

			try:
				subprocess.check_output('sudo docker-compose up -d', shell=True)
				ready = True
				break

			except Exception as e:
				print('\033[92mNo se pudieron iniciar los contenedores.\033[0m')

		else:
			print('-------------------------------------')
			print('\033[93mNO se encontro el archivo docker-compose.yml, si ya tiene uno debe colocarlo en este directorio o Generar un nuevo docker-compose.\033[0m')
			p = str(raw_input('\033[92mDesea generar un nuevo docker-compose? [y/n]: \033[0m'))
			if p is 'y' or p is 'Y':
				generateDockerCompose()

	if ready is True:

		print('-------------------------------------')
		q = str(raw_input('\033[92mDesea utilizar balancedores de carga? [y/n]: \033[0m'))

		if q is 'y' or q is 'Y':

			while True:

				try:
					print('-------------------------------------')
					num_lb = int(raw_input('\033[92mIndique el numero de balancedores (0 - salir): \033[0m'))
					if num_lb != 0:
						dockerBalance(num_lb)
					
					return False

				except Exception as e:
					print('\033[91mDebe ingresar un numero.\033[0m')


def stopOdoo():
	try:
		subprocess.check_output('sudo docker-compose stop', shell=True)
		print('-------------------------------------')
		print('\033[92mLos contenedores fueron detenidos correctamente.\033[0m')
	except Exception as e:
		print('\033[91mERROR al dentener los contenedores. \033[0m')


def deleteOdoo():
	confirm = str(raw_input('\033[91mEscriba "odoo10" para eliminar los contenedores: \033[0m'))
	if confirm == 'odoo10':
		try:
			subprocess.check_output('sudo docker-compose rm -sf', shell=True)
			print('-------------------------------------')
			print('\033[92mLos contenedores han sido eliminados correctamente.\033[0m ')
		except Exception as e:
			print('-------------------------------------')
			print('\033[91mERROR al eliminar los contenedores, recuerde que los contenedores NO deben estar corriendo. \033[0m')
	else:
		os.system('clear')
		run()


def dockerBalance(num_lb):
	try:
		subprocess.check_output('sudo docker-compose up --scale odoo10={} -d'.format(num_lb), shell=True)
		print('-------------------------------------')
		print('\033[92mBalancedores iniciados correctamente.\033[0m')
		
	except Exception as e:
		print('-------------------------------------')
		print('\033[91mNo se logro iniciar los balancedores.\033[0m')


def listDocker():
	try:
		subprocess.call('sudo docker-compose ps', shell=True)
	except Exception as e:
		print('-------------------------------------')
		print('\033[91mERROR al listar los contenedores.\033[0m')


def run():
	print('''\033[96m
a - Generar docker-compose.
b - Iniciar el contenedor de Odoo.
c - Detener los contenedores.
d - Eliminar los contenedores.
e - Usar balancedores de carga
f - Listar contenedores
0 - Salir
		\033[0m''')

	options = str(raw_input('\033[94mGenerador de contenedores de Odoo, que desea hacer?: \033[0m'))

	verify = verifyDockerCompose()

	if verify is True:

		if options is 'a':
			generateDockerCompose()

		elif options is 'b':
			startOdoo()

		elif options is 'c':
			stopOdoo()

		elif options is 'd':
			deleteOdoo()

		elif options is 'e':
			while True:
				try:
					print('-------------------------------------')
					num_lb = int(raw_input('\033[92mIndique el numero de balancedores (0 - salir): \033[0m'))
					if num_lb != 0:
						dockerBalance(num_lb)
					
					return False

				except Exception as e:
					print('\033[91mDebe ingresar un numero.\033[0m')

		elif options is 'f':
			listDocker()

		elif options is '0':
			print('\033[96mHasta pronto...\033[0m')
			exit()

		else:
			os.system('clear')
			print('-------------------------------------')
			print('\033[91mOpcion incorrecta.\033[0m')
			run()
		
		print('\033[96mHasta pronto..\033[0m')


if __name__ == '__main__':
	run()
